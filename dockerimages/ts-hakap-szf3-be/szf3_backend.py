from time import sleep
import os
import datetime


output_dir = "output_html"

if not os.path.exists(output_dir):
    os.mkdir(output_dir)

while True:
    with open(f'{output_dir}/index.html', 'w') as f:
        f.write(datetime.datetime.now().isoformat())
    sleep(0.5)